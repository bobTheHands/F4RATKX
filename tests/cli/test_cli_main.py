##############################################################################
# Copyright (C) 2020 - 2021 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of F4RATK.
#
# F4RATK is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from typing import Generator

from click.testing import CliRunner
from pytest import fixture

from f4ratk.cli.commands import main


class TestMainHelp:
    @fixture(scope='class', name='output')
    def given_help_option_when_invoked(self) -> Generator[str, None, None]:
        result = CliRunner().invoke(main, ['--help'])
        assert result.exit_code == 0
        yield result.output

    def should_display_help_option(self, output: str):
        assert '--help' in output

    def should_display_verbose_options(self, output: str):
        assert '-v, --verbose' in output
        assert 'Increase output verbosity.' in output

    def should_display_about_option(self, output: str):
        assert '--about' in output
        assert 'Display program information and exit.' in output

    def should_display_ticker_command(self, output: str):
        assert 'ticker' in output
        assert 'Analyze a ticker symbol.' in output

    def should_display_file_command(self, output: str):
        assert 'file' in output
        assert 'Analyze a CSV file.' in output

    def should_display_convert_command(self, output: str):
        assert 'convert' in output
        assert "Convert files to the 'file' command format." in output

    def should_display_history_command(self, output: str):
        assert 'history' in output
        assert "Display historic factor returns." in output

    def should_display_portfolio_command(self, output: str):
        assert 'portfolio' in output
        assert "Analyze a portfolio file." in output


class TestMainAbout:
    @fixture(scope='class', name='output')
    def given_help_option_when_invoked(self) -> Generator[str, None, None]:
        result = CliRunner().invoke(main, ['--about'])
        assert result.exit_code == 0
        yield result.output

    def should_display_copyright_notice(self, output: str):
        assert 'Copyright' in output

    def should_display_project_home(self, output: str):
        assert 'https://codeberg.org/toroettg/F4RATK' in output

    def should_display_license_notice(self, output: str):
        assert 'GNU Affero General Public License version 3' in output
