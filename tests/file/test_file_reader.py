##############################################################################
# Copyright (C) 2020 - 2021 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of F4RATK.
#
# F4RATK is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

from unittest.mock import Mock

from pandas import DataFrame, DatetimeIndex, Period, PeriodDtype, PeriodIndex
from pandas.tseries.offsets import BusinessDay
from pytest import approx

from f4ratk.data_reader import CsvFileReader
from f4ratk.domain import Currency, Frequency
from f4ratk.file.reader import FileContentReader, ValueFormat
from f4ratk.shared import Normalizer


def given_a_descending_date_index_when_reading_data_should_sort_result_ascending():
    def given():
        dates = ['2020-12-31', '2020-11-01', '2020-10-15']
        returns = (3.00, 2.00, 1.00)
        data = DataFrame(
            data={'Returns': returns},
            index=DatetimeIndex(data=dates, dtype='datetime64[ns]'),
        )

        return Mock(spec_set=CsvFileReader, read=Mock(return_value=data))

    mock_reader = given()

    result = FileContentReader(
        csv_reader=mock_reader,
        exchange_reader=None,
        currency=Currency.USD,
        value_format=ValueFormat.PRICE,
        normalizer=Normalizer(),
    ).read(start=None, end=None, frequency=Frequency.DAILY)

    assert len(result) == 2
    assert result.head(n=1).index[0] == Period('2020-11-01', 'B')
    assert result.tail(n=1).index[0] == Period('2020-12-31', 'B')


def given_a_daily_price_data_csv_file_when_reading_data_should_parse_first_column_to_daily_period_index():  # noqa: E501
    def given():
        dates = ['2020-12-31', '2020-11-01', '2020-10-15']
        returns = (3.00, 2.00, 1.00)
        data = DataFrame(
            data={'Returns': returns},
            index=DatetimeIndex(data=dates, dtype='datetime64[ns]'),
        )

        return Mock(spec_set=CsvFileReader, read=Mock(return_value=data))

    mock_reader = given()

    result = FileContentReader(
        csv_reader=mock_reader,
        exchange_reader=None,
        currency=Currency.USD,
        value_format=ValueFormat.PRICE,
        normalizer=Normalizer(),
    ).read(start=None, end=None, frequency=Frequency.DAILY)

    assert isinstance(result.index, PeriodIndex)
    assert result.index.dtype == PeriodDtype(freq=BusinessDay())
    assert list(result.columns) == ['Returns']


def given_a_daily_price_data_csv_file_when_reading_data_should_parse_second_column_to_relative_percentage_returns():  # noqa: E501
    def given():
        dates = ['2020-04-01', '2020-04-02', '2020-04-03']
        returns = (3.00, 2.00, 4.00)
        data = DataFrame(
            data={'Returns': returns},
            index=DatetimeIndex(data=dates, dtype='datetime64[ns]'),
        )

        return Mock(spec_set=CsvFileReader, read=Mock(return_value=data))

    mock_reader = given()

    result = FileContentReader(
        csv_reader=mock_reader,
        exchange_reader=None,
        currency=Currency.USD,
        value_format=ValueFormat.PRICE,
        normalizer=Normalizer(),
    ).read(start=None, end=None, frequency=Frequency.DAILY)

    assert result['Returns']['2020-04-02'] == approx(-33.33333, rel=0.0001)
    assert result['Returns']['2020-04-03'] == 100.0


def given_a_monthly_price_data_csv_file_when_reading_data_should_parse_first_column_to_monthly_period_index():  # noqa: E501
    def given():
        dates = ['2020-04', '2020-05', '2020-06']
        returns = (3.00, 2.00, 4.00)
        data = DataFrame(
            data={'Returns': returns},
            index=DatetimeIndex(data=dates, dtype='datetime64[ns]'),
        )

        return Mock(spec_set=CsvFileReader, read=Mock(return_value=data))

    mock_reader = given()

    result = FileContentReader(
        csv_reader=mock_reader,
        exchange_reader=None,
        currency=Currency.USD,
        value_format=ValueFormat.PRICE,
        normalizer=Normalizer(),
    ).read(start=None, end=None, frequency=Frequency.MONTHLY)

    assert result['Returns']['2020-05'] == approx(-33.33333, rel=0.0001)
    assert result['Returns']['2020-06'] == 100.0
