##############################################################################
# Copyright (C) 2020 - 2021 Tobias Röttger <dev@roettger-it.de>
#
# This file is part of F4RATK.
#
# F4RATK is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License version 3
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################

import json
from io import BytesIO

from flask import Response
from flask.testing import FlaskClient
from pandas_datareader._utils import RemoteDataError
from pandas_datareader.yahoo.daily import YahooDailyReader
from pytest import fixture
from pytest_mock import MockerFixture

from tests.conftest import FamaReaderFactory, YahooReaderFactory


@fixture(scope='function')
def setup_returns(
    fama_returns: FamaReaderFactory,
) -> None:
    fama_returns(('2020-05', 5), ('2020-06', 5))


def test_ticker_request(
    setup_returns: None,
    yahoo_returns: YahooReaderFactory,
    client: FlaskClient,
):
    yahoo_returns(
        ('2020-04-30', 3.0),
        ('2020-05-31', 1.5),
        ('2020-06-30', 2.0),
    )

    response: Response = client.get('/v0/tickers/AVDV;region=DEVELOPED-EX-US')

    assert response.status_code == 200
    assert response.headers['Content-Type'] == 'application/json'
    assert response.headers['Cache-Control'] == 'max-age=1209600'

    body = response.get_json()

    assert body['confidenceLevel'] == 0.95
    assert body['adjustedRSquared'] is not None
    assert body['coefficients'] is not None
    assert body['excessReturn'] is not None
    assert body['observations'] is not None
    assert body['period']['start'] == '2020-05'
    assert body['period']['end'] == '2020-06'


def given_unknown_ticker_symbol_when_ticker_request_received_should_respond_with_not_found_error(  # noqa: E501
    client: FlaskClient, mocker: MockerFixture
):
    mocker.patch(
        'f4ratk.ticker.reader.yahoo_reader',
        spec_set=YahooDailyReader,
        side_effect=RemoteDataError(),
    )

    response: Response = client.get('/v0/tickers/AVDV;region=DEVELOPED-EX-US')

    assert response.status_code == 404
    assert response.headers['Content-Type'] == 'application/json'

    body = response.get_json()

    assert body['errors'][0]['status'] == "404"
    assert body['errors'][0]['code'] == "NO_DATA"
    assert body['errors'][0]['title'] == "No data"
    assert body['errors'][0]['detail'] == "No data fetched for symbol 'AVDV'."


def test_files_request(setup_returns: None, client: FlaskClient):

    config = {
        'region': 'DEVELOPED-EX-US',
    }

    content = "\n".join(
        f"{date},{value}"
        for date, value in (
            ("2020-04", "20"),
            ("2020-05", "22.00"),
            ("2020-06", "23.00"),
        )
    )

    body = {
        'file': (
            BytesIO(content.encode()),
            'test_input.csv',
        ),
        'config': json.dumps(config),
    }

    response: Response = client.post(
        "/v0/files", data=body, content_type='multipart/form-data'
    )

    assert response.status_code == 200
    assert response.headers["Content-Type"] == "application/json"

    body = response.get_json()

    assert body['confidenceLevel'] == 0.95
    assert body['adjustedRSquared'] is not None
    assert body['coefficients'] is not None
    assert body['excessReturn'] is not None
    assert body['observations'] is not None
    assert body['period']['start'] == '2020-05'
    assert body['period']['end'] == '2020-06'
