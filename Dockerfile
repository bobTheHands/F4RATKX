FROM alpine:3.14.0 AS backend-build

WORKDIR /usr/src/app/

RUN set -xeo pipefail  \
  && apk add --no-cache git python3 py3-pip \
  && pip install pipenv-to-requirements

COPY Pipfile.lock ./

RUN set -xeo pipefail  \
  && pipenv_to_requirements -f

FROM python:3.9.6-slim-buster

WORKDIR /usr/src/app/

RUN set -xe pipefail \
  && pip install gunicorn==20.1.0

COPY --from=backend-build /usr/src/app/requirements.txt ./requirements.txt

RUN set -xe pipefail \
  && pip install -r requirements.txt

COPY ./f4ratk/ ./f4ratk/

CMD ["gunicorn", "f4ratk.web.server:create_app()"]
